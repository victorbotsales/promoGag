Promogag
--------

O projeto consiste em um sistema de publicação de promoções oferecidas por sites de vendas online e que permite a avaliação destas por parte dos usuários. Cada publicação representará um elemento independente no sistema, dotado de uma pontuação baseada na interação dos usuários com a publicação. A pontuação de cada publicação será utilizada para medir a inclinação dos usuários em relação a promoção e ordenar os elementos nas páginas de exibição de anúncios do sistema.

Usage
-----

Para fazer o DEPLOY na nuvem, você pode seguir este tutorial: https://drive.google.com/file/d/0B8dieMDZ9j-caWVOWmo4dkpadk0/view?usp=sharing

OBS: Recomendamos que baixe o PDF para visualizar melhor as Imagens dos Screenshots! ( Com o zoom 202% no Adobe Reader fica perfeito!)

OBS2: Os arquivos utilizados (projeto) que foi utilizado para subir na nuvem, foi desse branch aqui:
https://gitlab.com/victorbotsales/promoGag/tree/PromogagNaNuvem


---------------------------------------------------------------------------------------------------------------------------------------------------
A versão de deploy local e o passo-a-passo está localizada no branch Promogag_local: https://gitlab.com/victorbotsales/promoGag/tree/Promogag_local (sem o bower_components)
e completo neste outro git: https://gitlab.com/victorbotsales/promoGagDeployLocal/tree/master


        
      