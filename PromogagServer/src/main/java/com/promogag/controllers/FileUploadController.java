package com.promogag.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

@CrossOrigin (origins = "*")
@Controller
public class FileUploadController {

    @CrossOrigin (origins = "*")
    @RequestMapping(method = RequestMethod.POST, value = "/upload")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String handleFileUpload(
            @RequestParam("file") MultipartFile file,
            HttpServletRequest request) {
        if (file.getOriginalFilename().contains("/")) {
            throw new RuntimeException("Folder separators not allowed");
            //return "redirect:/";
        }

        if (!file.isEmpty()) {
            try {
                String realPath = request.getSession().getServletContext().getRealPath("/");
                String extensao = file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length - 1];
                String nomeArquivo = java.util.UUID.randomUUID() + "." + extensao;
                String caminhoArquivo = realPath + "//uploads//" + nomeArquivo;
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(new File(caminhoArquivo)));
                FileCopyUtils.copy(file.getInputStream(), stream);
                stream.close();
                return "{\"arquivo\": \"" + nomeArquivo + "\"}";
                //redirectAttributes.addFlashAttribute("message",
                //        "You successfully uploaded " + file.getOriginalFilename() + "!");
            }
            catch (Exception e) {
                throw new RuntimeException(e);
                //redirectAttributes.addFlashAttribute("message",
                //       "You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
            }
        }
        else {
            throw new RuntimeException(
                    "You failed to upload " + file.getOriginalFilename() + " because the file was empty");
        }

        //return "File uplodaded!";
    }

}
