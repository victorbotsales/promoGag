package com.promogag.repository;

import com.promogag.domain.Votacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by bot on 6/26/2016.
 */
public interface VotacaoRepository extends CrudRepository<Votacao, Long> {

    public List<Votacao> findByipAddressAndItemVotado(String ip, long item);
}
