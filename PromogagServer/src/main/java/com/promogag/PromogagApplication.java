package com.promogag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@SpringBootApplication
public class PromogagApplication  extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PromogagApplication.class, args);
    }

    // Provê a configuração da aplicação para o Tomcat (como se fosse um web.xml)
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PromogagApplication.class);
    }
}
