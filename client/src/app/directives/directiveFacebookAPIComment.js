//Código do link http://luisfbmelo.com/blog/2015/10/31/angularjs-facebook-comments/
// Facebook API Comment provido pelo facebook Developer não pega da "melhor forma" com o Angular!

var appDirectives = angular.module('myApp');

appDirectives.directive('facebookComments', ['$location', function ($location) {
    return {
        restrict: 'E',
        templateUrl: "scripts/directives/facebookComments.html",
        scope:{},
        replace: true,
        link: function(scope, el, attr){
            scope.curPath = $location.absUrl();
        }
    };
}]);

appDirectives.directive('dynFbCommentBox',['$timeout', function ($timeout) {
    function createHTML(href, numposts, colorscheme, width) {
        return '<div class="fb-comments" ' +
            'data-href="' + href + '" ' +
            'data-numposts="' + numposts + '" ' +
            'data-colorsheme="' + colorscheme + '" ' +
            'data-width="' + width + '">' +
            '</div>';
    }

    return {
        restrict: 'A',
        scope: {},
        link: function postLink(scope, elem, attrs) {
            //
            // Use timeout in order to be called after all watches are done and FB script is loaded
            //
            attrs.$observe('pageHref', function (newValue) {
                var href        = newValue;
                var numposts    = attrs.numposts    || 5;
                var colorscheme = attrs.colorscheme || 'light';
                var width = attrs.width || '100%';
                elem.html(createHTML(href, numposts, colorscheme, width));
                $timeout(function () {
                    if (typeof FB != 'undefined'){
                        FB.XFBML.parse(elem[0]);
                    }
                });
            });


        }
    };
}]);