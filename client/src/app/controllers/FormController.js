(function () {
    'use strict';
    angular
        .module('myApp')
        .controller('FormController', FormController);

    FormController.$inject = ['factItensPromocao', 'Upload', '$timeout', '$window'];

    function FormController(factItensPromocao, Upload, $timeout, $window) {

        // vm = representa o viewModel do MainPage ( John Papa Angular (controllerAs with vm) //
        var vm = this;

        vm.nomeArquivo = "";
        vm.habilitarUpload = false;
        vm.item = {"likes": 0, "dislikes": 0};
        vm.submeterPromocao = submeterPromocao;
        vm.uploadFiles = uploadFiles;


        /*
         Ativação dos Componentes!
         */

        setTimeout(activateComponents, 1);
        /*
         Função para ativar os componentes do Modal (Utilizado pelo login) e também pela navegação lateral (sideNav) utilizado no mobile!
         */
        function activateComponents() {
            $(document).ready(function () {
                // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
                $('.modal-trigger').leanModal();
                $('.button-collapse').sideNav();
                $('select').material_select();
            });
        }

        function reset(form) {
            if (form) {
                form.$setPristine();
                form.$setUntouched();
                $('form[name="enviarPromocaoForm"]').each(function () {
                    this.reset();
                });
            }
        }

        function uploadFiles(file) {
            if (file) {
                file.upload = Upload.upload({
                    url: 'http://52.67.95.20:8080/PromogagServer/upload',
                    headers: {'Content-Type': 'multipart/form-data'},
                    withCredentials: true,
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    vm.nomeArquivo = data.arquivo;
                    vm.habilitarUpload = true;
                }).error(function (e) {
                    console.log(e);
                    sweetAlert("Oops...", "Falha ao fazer upload, tente novamente!", "error");
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        console.log(response.status + ': ' + response.data);
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }
        }


        function submeterPromocao(itemPromocao, form) {
            var basePath = "uploads/";
            angular.extend(itemPromocao, vm.item);
            angular.extend(itemPromocao, {"pubDate": new Date()});
            itemPromocao.imagePath = basePath + vm.nomeArquivo;
            var precoAux = itemPromocao.preco.toString();
            precoAux = parseFloat(precoAux.replace('.', '').replace(',', '.').replace('R$', ''));
            console.log(precoAux);
            itemPromocao.preco = precoAux;
            factItensPromocao.getAccessToken()
                .then(function (aT) {
                    console.log(aT);
                    factItensPromocao.submeterPromocao().save({"accessToken": aT}, itemPromocao).$promise.then(
                        function (response) {
                            swal("Promoção adicionada com sucesso!", "", "success");
                            console.log('saveOK response');
                            vm.habilitarUpload = false;
                            angular.copy({}, itemPromocao);
                            reset(form);
                        },
                        function (response) {
                            console.log("Error: " + response.status + " " + response.statusText);
                            sweetAlert("Oops...", "Ocorreu um erro no nosso servidor, tente novamente por favor!", "error")
                            //Vai ter que da o reload, pois perdeu o acessToken!
                            // $timeout($window.location.reload(), 3000);
                        }
                    )

                }, function () {
                    sweetAlert("Oops...", "Você não está logado, por favor logar!", "error");

                })


        }
    }

})();