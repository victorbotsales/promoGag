'use strict';

/**
 *
 * Main module of the application.
 */
angular
    .module('myApp', ['ngRoute', 'ngResource', 'ngFileUpload','angular-price-format','angularUtils.directives.dirPagination']);
