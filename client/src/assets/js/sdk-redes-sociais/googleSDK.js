 function onSuccess(googleUser) {
            console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
        }

        function onFailure(error) {
            console.log(error);
        }

        function renderButton() {
            gapi.signin2.render('my-signin2', {
                'scope': 'profile email'
                , 'width': 122
                , 'height': 50
                , 'longtitle': false
                , 'theme': 'light'
                , 'onsuccess': onSuccess
                , 'onfailure': onFailure
            });
        }